package com.salvation.wayofthecross;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.view.View;

import com.salvation.wayofthecross.dialog.Dialog;

import static com.salvation.wayofthecross.HelperMethods.startNextActivity;

public class ChooseLanguageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);

        String[] arr = getResources().getStringArray(R.array.languages);

        AppCompatButton btn_language = findViewById(R.id.btn_language);
        btn_language.setOnClickListener(v -> {
            Dialog.showSpinnerDialog(btn_language, "Choose Language", arr);
        });

        findViewById(R.id.btn_next).setOnClickListener(v -> {
            startNextActivity(  this, StationNumberListActivity.class);
        });
    }
}