package com.salvation.wayofthecross;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        runSplash();
    }

    private void runSplash() {
        new Handler().postDelayed(() -> {
//            Class<?> nextClass;
//            if (userSession.shouldSelectLanguage()){
//                nextClass = ChooseLanguageActivity.class;
//            }else if (userSession.shouldAuthenticate()){
//                nextClass = LoginActivity.class;
//            }else {
//                nextClass = DashboardWallet.class;
//            }
            Intent intent = new Intent(this, ChooseLanguageActivity.class);
            startActivity(intent);
            finish();
        },2500);
    }

}