package com.salvation.wayofthecross;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class HelperMethods {
    public static void startNextActivity(Activity activity, Class<?> cls){
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);
    }

    public static void log(Object obj){
        Log.i("CROSS", obj.toString());
    }
}
